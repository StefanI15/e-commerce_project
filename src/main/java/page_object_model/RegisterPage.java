package page_object_model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class RegisterPage {

    private final WebDriver driver;

    public static String registerURL = "https://automationexercise.com/signup";

    public By registerUsername = By.xpath("//*[@name='name']");
    public By registerEmailAddressSingUp = By.xpath("//*[@data-qa='signup-email']");
    public By registerSignUpButton = By.xpath("//button[@data-qa='signup-button']");

    public By titleForm = By.xpath("//h2[@class=\"title text-center\" and @style=\"color: #FE980F;\"]/b[text()=\"Enter Account Information\"]\n");
    public By misterRadio = By.cssSelector("#id_gender1");
    public By name = By.cssSelector("#name");
    public By email = By.cssSelector("#email");
    public By password = By.xpath("//input[@type='password']");
    public By dayOfBirth = By.cssSelector("#days");
    public By monthOfBirth = By.cssSelector("#uniform-months");
    public By yearOfBirth = By.cssSelector("#uniform-years");
    public By newsletter = By.cssSelector("newsletter");
    public By firstname = By.xpath("//input[@data-qa = 'first_name']");
    public By lastname = By.xpath("//input[@data-qa = 'last_name']");
    public By companyName = By.xpath("//input[@data-qa = 'company']");
    public By address1 = By.xpath("//input[@data-qa = 'address']");
    public By address2 = By.xpath("//input[@data-qa = 'address2']");
    public By country = By.cssSelector("#country");
    public By state = By.xpath("//input[@data-qa='state']");
    public By city = By.xpath("//input[@data-qa='city']");
    public By zipcode = By.xpath("//input[@data-qa='zipcode']");
    public By mobileNumber = By.xpath("//input[@data-qa='mobile_number']");
    public By createAccountButton = By.xpath("//button[@data-qa='create-account']");
    public By closeAdv = By.cssSelector("#cbb");



    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    //Actions
    public void registerWithMandatoryFields(String pass, String firstname, String lastname, String address1,
                                            String country, String state, String city, String zipcode,
                                            String mobileNumber) {
        enterPassword(pass);
        enterFirstName(firstname);
        enterLastName(lastname);
        enterFirstAddress(address1);
        selectCountry(country);
        enterState(state);
        enterCity(city);
        enterZipcode(zipcode);
        enterMobileNumber(mobileNumber);
    }
    public void clickOnMisterRadioButton() {
        driver.findElement(this.misterRadio).click();
    }
    public String getName() {
        return driver.findElement(name).getText();
    }
    public String getEmail() {
        return driver.findElement(email).getText();
    }

    public void enterPassword(String pass) {
        driver.findElement(this.password).sendKeys(pass);
    }
    public void selectDayOfBirth(String day) {
        findDayDropDown().selectByVisibleText(day);
    }
    public List<String> getSelectedDays() {
        List<String> selectedValues = new ArrayList<>();
        List<WebElement> selectedElements = findDayDropDown().getAllSelectedOptions();
        for(WebElement element : selectedElements) {
            selectedValues.add(element.getText());

        }
        return selectedValues;
    }
    private Select findDayDropDown() {
        return new Select(driver.findElement(dayOfBirth));
    }
    public void selectMonthOfBirth(String month) {
        findMonthDropDown().selectByVisibleText(month);
    }
    public List<String> getSelectedMonths() {
        List<String> selectedValues = new ArrayList<>();
        List<WebElement> selectedElements = findMonthDropDown().getAllSelectedOptions();
        for(WebElement element : selectedElements) {
            selectedValues.add(element.getText());

        }
        return selectedValues;
    }
    private Select findMonthDropDown() {
        return new Select(driver.findElement(monthOfBirth));
    }
    public void selectYearOfBirth(String year) {
        findYearDropDown().selectByVisibleText(year);
    }
    public List<String> getSelectedYears() {
        List<String> selectedValues = new ArrayList<>();
        List<WebElement> selectedElements = findYearDropDown().getAllSelectedOptions();
        for(WebElement element : selectedElements) {
            selectedValues.add(element.getText());

        }
        return selectedValues;
    }
    private Select findYearDropDown() {
        return new Select(driver.findElement(yearOfBirth));
    }
    public void clickOnCheckboxNewsletter() {
        driver.findElement(newsletter).click();
    }
    public void enterFirstName(String firstName) {
        driver.findElement(this.firstname).sendKeys(firstName);
    }
    public void enterLastName(String lastName) {
        driver.findElement(this.lastname).sendKeys(lastName);
    }
    public void enterCompanyName(String companyName) {
        driver.findElement(this.companyName).sendKeys(companyName);
    }
    public void enterFirstAddress(String address1) {
        driver.findElement(this.address1).sendKeys(address1);
    }
    public void enterSecondAddress(String address2) {
        driver.findElement(this.address2).sendKeys(address2);
    }
    public void selectCountry(String country) {
        findCountryDropDown().selectByVisibleText(country);
    }
    public List<String> getSelectedCountries() {
        List<String> selectedValues = new ArrayList<>();
        List<WebElement> selectedElements = findCountryDropDown().getAllSelectedOptions();
        for(WebElement element : selectedElements) {
            selectedValues.add(element.getText());

        }
        return selectedValues;
    }
    private Select findCountryDropDown() {
        return new Select(driver.findElement(country));
    }
    public void enterState(String state) {
        driver.findElement(this.state).sendKeys(state);
    }
    public void enterCity(String city) {
        driver.findElement(this.city).sendKeys(city);
    }
    public void enterZipcode(String zipcode) {
        driver.findElement(this.zipcode).sendKeys(zipcode);
    }
    public void enterMobileNumber(String mobileNumber) {
        driver.findElement(this.mobileNumber).sendKeys(mobileNumber);
    }
    // Verifications
    public String titleFormIsDisplayed() {
        return driver.findElement(titleForm).getText();

    }

    public String getFirstnameValidationMessage() {
        return driver.findElement(firstname).getAttribute("validationMessage");
    }
    public String getCityValidationMessage() {
        return driver.findElement(city).getAttribute("validationMessage");
    }
    public void enterCredentialsAndLoginRegister(String username, String email) {
        enterRegisterUserName(username);
        enterRegisterEmailAddressSignUp(email);
        clickOnSignUpButtonRegister();
    }
    public void enterRegisterUserName(String userName) {
        driver.findElement(registerUsername).sendKeys(userName);
    }

    public void enterRegisterEmailAddressSignUp(String email) {
        driver.findElement(registerEmailAddressSingUp).sendKeys(email);
    }

    public void clickOnSignUpButtonRegister() {
        driver.findElement(registerSignUpButton).click();
    }
    public void clickOnCreateAccountButton() {
        driver.findElement(createAccountButton).click();
    }
    public void clickOnAdvertisingButton() {
        driver.findElement(closeAdv).click();
    }
}
