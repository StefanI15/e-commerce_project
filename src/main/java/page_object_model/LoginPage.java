package page_object_model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

        private final WebDriver driver;

        public static String loginURL = "https://automationexercise.com/login";

        // Locators
        public By nameSingup = By.xpath("//*[@name='name']");
        public By emailAddressSingUp = By.xpath("//*[@data-qa='signup-email']");
        public By emailAddressLogin = By.xpath("//input[@data-qa='login-email']");
        public By loginPassword = By.xpath("//input[@data-qa='login-password']");
        public By loginButton = By.xpath("//button[@data-qa='login-button']");
        public By signUpButton = By.xpath("//*[@data-qa='signup-button']");
        public By errorMessage = By.xpath("//p[contains(text(), 'Email Address already exist!') and @style=\"color: red;\"]");

        // Constructor
        public LoginPage(WebDriver driver) {
                this.driver = driver;
        }
        // Actions
        public void enterUserName(String userName) {
                driver.findElement(nameSingup).sendKeys(userName);
        }

        public void enterEmailAddressSignUp(String email) {
                driver.findElement(emailAddressSingUp).sendKeys(email);
        }

        public void clickOnSignUpButton() {
                driver.findElement(signUpButton).click();
        }
        public void enterEmailAddressLogin(String email) {
                driver.findElement(emailAddressLogin).sendKeys(email);
        }
        public void enterPassword(String password) {
                driver.findElement(loginPassword).sendKeys(password);
        }
        public void clickOnLoginButton() {
                driver.findElement(loginButton).click();
        }

        // Verifications
        public String errorMessageIsDisplayed() {
                return driver.findElement(errorMessage).getText();
        }

        public boolean signUpButtonIsDisplayed() {
                return driver.findElement(signUpButton).isDisplayed();
        }
        public String getPasswordValidationMessage() {
                return driver.findElement(emailAddressSingUp).getAttribute("validationMessage");

        }
        public String getUsernameValidationMessage() {
                return driver.findElement(nameSingup).getAttribute("validationMessage");
        }
        public void loginButtonIsDisplayed() {
                driver.findElement(loginButton).isDisplayed();
        }

}
