package page_object_model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountCreated {

    private final WebDriver driver;

    public static String accountCreatedURL = "https://automationexercise.com/account_created";

    public AccountCreated(WebDriver driver) {
        this.driver = driver;
    }

    public By titleAccountCreated = By.xpath("//h2[@class='title text-center']/b[text()='Account Created!']");
    public By firstParagraph = By.xpath("//div[@class=\"col-sm-9 col-sm-offset-1\"]//p[contains(text(), \"Congratulations! Your new account has been successfully created!\")]\n");
    public By secondParagraph = By.xpath("//div[@class=\"col-sm-9 col-sm-offset-1\"]//p[contains(text(), \"You can now take advantage\")]\n");
    public By continueButton = By.cssSelector("a.btn.btn-primary[data-qa=\"continue-button\"]");

    public String getAccountCreatedTitle() {
        return driver.findElement(titleAccountCreated).getText();
    }
    public String getCongratulationsMessage() {
        return driver.findElement(firstParagraph).getText();
    }
    public String getDescriptionMessage() {
        return driver.findElement(secondParagraph).getText();
    }
    public boolean continueButtonIsEnabled() {
        return driver.findElement(continueButton).isEnabled();
    }
    public void clickOnContinueButton() {
        driver.findElement(continueButton).click();
    }
}
