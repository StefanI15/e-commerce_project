package tests;

import org.junit.Assert;
import org.junit.Test;
import page_object_model.LoginPage;
import page_object_model.RegisterPage;
import utils.BaseTest;

import java.util.List;

public class SignUpNegativeTests extends BaseTest {

    String userName = "stelian";
    String emailAddress = "test@gmail.com";




    @Test
    public void attemptToSignUpWithExistingEmailAddress() {
        driver.get(LoginPage.loginURL);
        LoginPage loginPage = new LoginPage(driver);

        loginPage.enterUserName(userName);
        loginPage.enterEmailAddressSignUp(emailAddress);
        loginPage.clickOnSignUpButton();
        Assert.assertEquals("I am on form page", "Email Address already exist!", loginPage.errorMessageIsDisplayed());
    }
    @Test
    public void attemptToSignUpOnlyWithUserName() {
        driver.get(LoginPage.loginURL);
        LoginPage loginPage = new LoginPage(driver);

        loginPage.enterUserName(userName);
        loginPage.clickOnSignUpButton();
        //Assert.assertTrue("SignUp button is not displayed", loginPage.signUpButtonIsDisplayed());
        Assert.assertEquals("Validation message is not displayed", "Please fill out this field.",
                loginPage.getPasswordValidationMessage());
    }
    @Test
    public void loginWithCorrectCredentials(){
        String user = "stelian";
        String email = "wolverine@gmail.com";
        driver.get(LoginPage.loginURL);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterUserName(user);
        loginPage.enterEmailAddressSignUp(email);
        loginPage.clickOnSignUpButton();
    }
    @Test
    public void selectDays() {
        String dayTwenty = "20";
        RegisterPage registerPage = new RegisterPage(driver);
        loginWithCorrectCredentials();


        registerPage.selectDayOfBirth(dayTwenty);
        List<String> selectedDays = registerPage.getSelectedDays();
        Assert.assertEquals("Incorrect number of selections", selectedDays.size(), 1);
        Assert.assertTrue("My option is not selected", selectedDays.contains(dayTwenty));
    }
}
