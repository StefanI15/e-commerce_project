package stepdefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import page_object_model.AccountCreated;
import page_object_model.RegisterPage;

import static stepdefinition.Hooks.driver;
public class RegisterStepsPage {
    RegisterPage registerPage;
    AccountCreated accountCreated;

    @Given("Go to Register page")
    public void beOnSignupPage() {
        driver.get(RegisterPage.registerURL);
        registerPage = new RegisterPage(driver);
    }



    @Then("firstname tooltip validation message is displayed")
    public void firstnameTooltipValidationMessageIsDisplayed() {
        System.out.println("Firstname tooltip validation message is displayed");
        Assert.assertEquals("Firstname tooltip validation message is not displayed", "Please fill out this field.",
                registerPage.getFirstnameValidationMessage());
    }

    @When("enter password {string}")
    public void enterPassword(String password) {
        System.out.println("Enter password");
        registerPage.enterPassword(password);

    }

    @And("enter first name {string}")
    public void enterFirstName(String firstName) {
        System.out.println("Enter first name");
        registerPage.enterFirstName(firstName);
    }

    @And("enter last name {string}")
    public void enterLastName(String lastName) {
        System.out.println("Enter last name");
        registerPage.enterLastName(lastName);
    }

    @And("enter first address {string}")
    public void enterFirstAddress(String firstAddress) {
        System.out.println("Enter first address");
        registerPage.enterFirstAddress(firstAddress);
    }

    @And("select United States for country {string}")
    public void selectUnitedStatesForCountry(String country) {
        registerPage.selectCountry(country);
    }

    @And("enter state {string}")
    public void enterState(String state) {
        registerPage.enterState(state);
    }

    @And("enter city {string}")
    public void enterCity(String city) {
        registerPage.enterCity(city);
    }

    @And("enter zipcode {string}")
    public void enterZipcode(String zipcode) {
        registerPage.enterZipcode(zipcode);
    }

    @And("enter mobile number {string}")
    public void enterMobileNumber(String mobileNumber) {
        registerPage.enterMobileNumber(mobileNumber);
    }

    @And("enter register username {string}")
    public void enterRegisterUsername(String userNameRegister) {
        registerPage.enterRegisterUserName(userNameRegister);
    }

    @And("enter register email address {string}")
    public void enterRegisterEmailAddress(String emailRegister) {
        registerPage.enterRegisterEmailAddressSignUp(emailRegister);
    }

    @And("click on Signup button register")
    public void clickOnSignupButtonRegister() {
        registerPage.clickOnSignUpButtonRegister();
    }

    @And("click on create account button")
    public void clickOnCrateAccountButton() {
        registerPage.clickOnCreateAccountButton();
    }

    @Then("city tooltip validation message is displayed")
    public void cityTooltipValidationMessageIsDisplayed() {
        System.out.println("City tooltip validation message is displayed");
        Assert.assertEquals("City tooltip validation message is not displayed", "Please fill out this field.",
                registerPage.getCityValidationMessage());
    }

    @And("close advertising")
    public void closeAdvertising() {
        registerPage.clickOnAdvertisingButton();
    }

    @And("scroll down until button is visible")
    public void scrollDownUntilButtonIsVisible() {
        By createAccountButton = By.xpath("//button[@data-qa='create-account']");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(createAccountButton));
    }

    @When("enter all mandatory fields {string} {string} {string} {string} {string} {string} {string} {string} {string}")
    public void enterAllMandatoryFields(String pass, String firstname, String lastname,
                                        String address1, String country, String state, String city, String zipcode,
                                        String mobileNumber) {
        registerPage.registerWithMandatoryFields(pass, firstname,lastname, address1, country, state, city, zipcode, mobileNumber);
    }

    @Then("account created is displayed")
    public void accountCreatedIsDisplayed() {
        System.out.println("Account Created message is displayed");
        Assert.assertEquals("The Account Created title is not displayed", "ACCOUNT CREATED!", accountCreated.getAccountCreatedTitle());
    }
}
