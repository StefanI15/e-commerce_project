package stepdefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import page_object_model.AccountCreated;
import page_object_model.HomePage;

import static stepdefinition.Hooks.driver;

public class AccountCreatedStepsPage {

    AccountCreated accountCreated;

    @Given("Be on account created page")
    public void beOnAccountCreatedPage() {
        driver.get(AccountCreated.accountCreatedURL);
        accountCreated = new AccountCreated(driver);
    }

    @Then("congratulations paragraph is displayed")
    public void congratulationsParagraphIsDisplayed() {
        System.out.println("Congratulations message is displayed");
        Assert.assertEquals("Congratulations message is not displayed", "Congratulations! Your new account has been successfully created!",
                accountCreated.getCongratulationsMessage());
    }

    @Then("description message is displayed")
    public void descriptionMessageIsDisplayed() {
        System.out.println("Short description message is displayed");
        Assert.assertEquals("Short description message is not displayed", "You can now take advantage of member privileges to enhance your online shopping experience with us.",
                accountCreated.getDescriptionMessage());
    }

    @Then("continue button is enabled")
    public void continueButtonIsEnabled() {
        System.out.println("Continue button is enabled");
        Assert.assertTrue("Continue button is not enabled", accountCreated.continueButtonIsEnabled());
    }

    @When("click on continue button")
    public void clickOnContinueButton() throws InterruptedException {
        accountCreated.clickOnContinueButton();
        Thread.sleep(7000);
    }

    @Then("logout button is displayed")
    public void logoutButtonIsDisplayed() {
        HomePage homePage = new HomePage(driver);
        Assert.assertTrue("Logout button is not displayed", homePage.logoutButtonIsDisplayed());

    }
}
