package stepdefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import page_object_model.HomePage;

import static stepdefinition.Hooks.driver;

public class HomeStepsPage {

    HomePage homePage;

    @Given("be on home page")
    public void beOnHomePage() {
        driver.get(HomePage.homePageURL);
        homePage = new HomePage(driver);
    }

    @When("click on logout button")
    public void clickOnLogoutButton() {
        homePage.clickOnLogoutButton();
    }

    @Then("login button is displayed")
    public void loginButtonIsDisplayed() {
        Assert.assertTrue("Login button is not displayed", homePage.loginButtonIsDisplayed());
    }

    @Then("logout user")
    public void logoutUser() {
        Assert.assertTrue("Logout text is displayed", homePage.logoutTextUser());
    }
}
