package stepdefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import page_object_model.LoginPage;
import page_object_model.RegisterPage;

import java.util.List;

import static stepdefinition.Hooks.driver;


public class LoginStepsForSignup{

    LoginPage loginPage;

    @Given("Be on Login page")
    public void beOnLoginPage() {
        driver.get(LoginPage.loginURL);
        loginPage = new LoginPage(driver);
    }

    @When("enter username {string}")
    public void enterUsername(String username) {
        System.out.println("Enter username");
        loginPage.enterUserName(username);

    }

    @When("enter email address {string}")
    public void enterEmailAddress(String emailaddress) {
        System.out.println("Enter email address");
        loginPage.enterEmailAddressSignUp(emailaddress);
    }

    @When("click on Signup button")
    public void clickOnSignupButton() {
        System.out.println("Click on Signup button");
        loginPage.clickOnSignUpButton();
    }
    @Then("title form is displayed")
    public void titleTextFromFormIsDisplayed() {
        System.out.println("Title form is displayed");
        RegisterPage registerPage = new RegisterPage(driver);
        Assert.assertEquals("The title signup form is not displayed", "ENTER ACCOUNT INFORMATION", registerPage.titleFormIsDisplayed());

    }

    @Then("signup button is still displayed")
    public void signupButtonIsStillDisplayed() {
        System.out.println("Signup button is displayed");
        loginPage.enterUserName("GenerativeAI");
        Assert.assertTrue("Signup button is not displayed", loginPage.signUpButtonIsDisplayed());
    }

    @Then("error message is displayed")
    public void errorMessageIsDisplayed() {
        System.out.println("Error message is displayed");
        Assert.assertEquals("Error message is not displayed", "Email Address already exist!",
                loginPage.errorMessageIsDisplayed());
    }

    @Then("password tooltip validation message is displayed")
    public void validationMessageIsDisplayed() {
        System.out.println("Password tooltip validation message is displayed");
        Assert.assertEquals("Password tooltip validation message is not displayed", "Please fill out this field.",
                loginPage.getPasswordValidationMessage());
    }

    @Then("username tooltip validation message is displayed")
    public void usernameValidationMessageIsDisplayed() {
        System.out.println("Username tooltip validation message is displayed");
        Assert.assertEquals("Username tooltip validation message is not displayed", "Please fill out this field.",
                loginPage.getUsernameValidationMessage());
    }

    @When("enter correct credentials")
    public void enterCorrectCredentials(List<String> credentials) {
        loginPage.enterEmailAddressLogin(credentials.get(0));
        loginPage.enterEmailAddressLogin(credentials.get(1));
        loginPage.clickOnLoginButton();
    }
}
