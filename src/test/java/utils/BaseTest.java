package utils;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BaseTest {

    protected static WebDriver driver;

    @Before
    public void setUp() throws IOException {
        System.out.println("Setup method");
        InputStream input = new FileInputStream("src/test/resources/default.properties");
        Properties properties = new Properties();
        properties.load(input);

        String browser =properties.getProperty("browser");
        driver = WebDriverFactory.getDriver(browser);
    }

    @After
    public void cleanBrowser() {
        System.out.println("Cleanup method");
        driver.quit();
    }
}
