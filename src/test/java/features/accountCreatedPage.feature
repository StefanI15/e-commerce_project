@UI @Positive
  Feature: Account Created test scenarios

    Scenario: Verify that first and second paragraphs are displayed
      Given Be on account created page

      Then congratulations paragraph is displayed
      Then description message is displayed

    Scenario: Verify that Continue button is enabled
      Given Be on account created page

      Then continue button is enabled

    Scenario: Verify that Home page is displayed by clicking on Continue button
      Given Be on account created page

      When click on continue button
      Then logout button is displayed