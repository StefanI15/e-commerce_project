@UI @Negative

Feature: Register negative test scenarios

  Scenario: Register on page with mandatory fields apart of 'First name'
    Given Go to Register page
    And enter register username "ChatBot"
    And enter register email address "wolverine@gmail.com"
    And click on Signup button register

    When enter password "wolverine123"
    And enter last name "Arsenie"
    And enter first address "4 Halifax Court"
    And select United States for country "United States"
    And enter state "California"
    And enter city "Los Angeles"
    And enter zipcode "90003"
    And scroll down until button is visible
    And enter mobile number "123123123"
    And click on create account button
    Then firstname tooltip validation message is displayed

  Scenario: Signup on page with mandatory fields apart of 'City'
    Given Go to Register page
    And enter register username "ChatBot"
    And enter register email address "wolverine@gmail.com"
    And click on Signup button register

    When enter password "wolverine123"
    And enter first name "Stefan"
    And enter last name "Arsenie"
    And enter first address "4 Halifax Court"
    And select United States for country "United States"
    And enter state "California"
    And enter zipcode "90003"
    And scroll down until button is visible
    And enter mobile number "123123123"
    And click on create account button
    Then city tooltip validation message is displayed