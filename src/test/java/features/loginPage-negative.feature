@UI @Negative
Feature: Signup negative test scenarios

  Scenario: Signup by entering only username
    Given Be on Login page

    When enter username "ChatBot"
    And click on Signup button
    Then password tooltip validation message is displayed

  Scenario: Signup by entering only email address
    Given Be on Login page

    When enter email address "wolverine@gmail.com"
    And click on Signup button
    Then username tooltip validation message is displayed

  Scenario: Signup by entering an email address which already exists
    Given Be on Login page

    When enter username "ChatBot"
    And enter email address "test@gmail.com"
    And click on Signup button
    Then error message is displayed

  Scenario Outline: Signup with incorrect format email
    Given Be on Login page

    When enter username "ChatBot"
    And enter email address "<emailAddress>"
    And click on Signup button
    Then signup button is still displayed

    Examples:
      | emailAddress        |
      | wolverinegmail.com  |
      | wolverine@.com      |
      | wolverine@gmail.    |
      | wolverine.gmail@com |
      | wolverine@gmailcom  |
