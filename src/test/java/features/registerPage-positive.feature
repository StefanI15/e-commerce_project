@UI @Positive
Feature: Register positive test scenarios

  Scenario: Register on page with only mandatory fields
    Given Be on Register page
    And enter username GenerativeAI
    And enter email address wolverine@gmail.com
    And click on Signup button

    Given Go to Register page
    And enter register username "ChatBot"
    And enter register email address "wolverine@gmail.com"
    And click on Signup button register

    When enter all mandatory fields "wolverine123" "Stefan" "Arsenie" "4 Halifax Court" "United States" "California" "Los Angeles" "90003" "123123123"
    And click on create account button
    Then account created is displayed

