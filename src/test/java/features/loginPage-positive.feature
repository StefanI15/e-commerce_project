@UI @Positive
Feature: Signup positive test scenarios

  @Signup
  Scenario: Signup on page with correct name and email address
    Given Be on Login page

    When enter username "ChatBot"
    And enter email address "wolverine@gmail.com"
    And click on Signup button
    Then title form is displayed

  @Login
  Scenario: Login with correct emailAddress and Password
    Given Be on Login page

    When enter correct credentials
    | wolverine@gmail.com |
    | wolverine123        |
    Then logout button is displayed

